# config valid only for Capistrano 3.2.1
lock '3.2.1'

set :application, 'studynet'
set :repo_url, 'git@bitbucket.org:roryhughes/study_network.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/home/rory/www/sn'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
set :linked_files, %w{config/database.yml config/keys.yml}

# Default value for linked_dirs is []
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

set :assets_roles, [:app]
set :migration_role, :db

set :rvm1_ruby_version, "2.1.6"
before 'deploy', 'rvm1:install:ruby'

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      execute :touch, release_path.join('tmp/restart.txt')
      # execute "sudo service nginx restart"
    end
  end

  after :publishing, :restart

  # after :restart, :clear_cache do
  #   on roles(:web), in: :groups, limit: 3, wait: 10 do
  #     # Here we can do anything such as:
  #     # within release_path do
  #     #   execute :rake, 'cache:clear'
  #     # end
  #   end
  # end

  before :restart, :nginx_symlink

  desc "Recreate nginx.conf symlink"
  task :nginx_symlink do
    on roles(:app) do
      execute "sudo rm -f /opt/nginx/conf/nginx.conf && sudo ln -s #{release_path}/config/nginx.conf /opt/nginx/conf/nginx.conf"
    end
  end

  desc "Uploads YAML files."
  task :upload_yml do
    on roles(:all) do
      upload!("./config/keys_prod.yml", "#{shared_path}/config/keys.yml")
      upload!("./config/prod_database.yml", "#{shared_path}/config/database.yml")
    end
  end

  after :started, :upload_yml

  desc "Heartbleed..."
  task :upgrade_system do
    on roles(:all) do
      execute "sudo apt-get update && sudo apt-get upgrade"
    end
  end

end
