# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
set :output, "/home/rory/sn_jobs.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#

# Learn more: http://github.com/javan/whenever
every 1.minute, :roles => [:db] do
  # Must be run in this order becuase user karma looks at votes on each note
  runner "Note.cache_votes && User.cache_karma"
end
every 1.minutes, :roles => [:db] do
  runner "Note.count_page_views"
end