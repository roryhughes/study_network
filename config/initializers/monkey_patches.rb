class ActiveRecord::Base
  def self.job_msg(msg)
    job_name = caller_locations(1,1)[0].label
    str = "#{Time.now.asctime} - Job: #{self.to_s}.#{job_name}, #{msg}"
    puts str
    str
  end
    
  def self.sexcept(*columns)
    select(column_names - columns.map(&:to_s))
  end
end