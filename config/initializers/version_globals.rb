$version_hsh = YAML.load_file(Rails.root.join("change_log.yml"))
$version_number = $version_hsh["versions"][0]["version_str"]
$last_release_date = $version_hsh["versions"][0]["date"].strftime("%A, %F")

$hostname = `hostname`

if Rails.env.development?
  $dev_info = {
    git_branch: `git status | sed -n 1p`.split(" ").last,
    commit_name:  `git log | sed -n 5p`.chomp.split.join(" ")
  }
  puts "StudyNet version #{$version_number}"
  # ActiveRecordQueryTrace.enabled = true
end