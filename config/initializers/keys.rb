require "yaml"
$keys = YAML::load_file(File.expand_path(Rails.root.join("config/keys.yml")))

Recaptcha.configure do |config|
  config.public_key  = $keys["recaptcha"]["public_key"]
  config.private_key = $keys["recaptcha"]["private_key"]
  # config.proxy = 'http://myproxy.com.au:8080'
  config.use_ssl_by_default = true
end