StudyNetwork::Application.routes.draw do
  devise_for :users, :skip => [:sessions, :registrations]
  # Customizing devise
  as :user do
    # Sessions
    match "login" =>        "sessions#new", as: :new_user_session, via: [:get, :post] # sending to custom sessions for modal option
    post "login_user" =>    "sessions#create", as: :user_session
    delete "logout_user" => "sessions#destroy", as: :destroy_user_session

    # Registrations
    get "/register" =>    "registrations#new", as: :new_user_registration
    get "/u/edit" =>      "registrations#edit", as: :edit_user_registration
    post "/register" =>   "registrations#create", as: :user_registration
    patch "/register" =>  "registrations#update"
    put "/register" =>    "registrations#update"
    delete "/register" => "registrations#destroy"

    get "/u/delete_account" => "registrations#delete_account", via: :get

    get "/u/:id" => "registrations#show", as: :user
  end

  resources :n, as: :notes, controller: :notes do
    member do
      get "forks"
    end
  end

  namespace :admin do
    scope "n", as: "note" do
      # resources :notes, as: :n
      get ":id" =>             "notes#show"
      patch ":id/hide" =>      "notes#hide", as: :hide
      patch ":id/unhide" =>    "notes#unhide", as: :unhide
      patch ":id/feature" =>   "notes#feature", as: :feature
      patch ":id/unfeature" => "notes#unfeature", as: :unfeature
    end
  end

  match "goals/info" => "goals#info", as: :goals_info, via: [:get, :post]
  resources :goals do
    member do
      post   "t" =>    :create_todo
      put    ":tid" => :update_todo
      delete ":tid" => :destroy_todo
    end
  end

  get "/notifications" =>       "notifications#index", as: :notifications
  get "/notifications/count" => "notifications#count"
  get "/notifications/:id" =>   "notifications#show", as: :notification
  
  post "/v/:model/:id/:value" => "votes#create"

  # post "/modal/:modal_type" => "home#modal" # Legacy modal system :(

  get "/about" =>     "home#about"
  get "/changelog" => "home#changelog"

  get "/nw" => "home#marked_as_nw", as: :nw

  root "home#index"

  get "bgoals" => "home#beta"

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
