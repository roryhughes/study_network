# OLD, cachers now in models

# $stdout.sync = true
# namespace :cacher do
#   puts "Time: #{Time.now}"

#   desc "Cache votes in notes"
#   task :note_votes => :environment do
#     print "Caching votes into notes: "

#     to_cache = Vote.where(votable_type: "Note").not_cached
#     puts "#{to_cache.length} votes"
#     next unless to_cache.any?

#     grouped = to_cache.group_by(&:votable_id)
#     puts "#{grouped.length} notes"
#     grouped.each do |note_id, votes|
#       note = Note.find_by_id(note_id)
#       note.vote_cache += votes.length
#       note.save
#     end

#     to_cache.update_all(cached: true)
#     puts "votes marked as cached"
#   end

#   desc "Cache user Karma"
#   task :user_karma => :environment do
#     print "Caching user karma: "

#     to_cache = Vote.not_karma_cached.includes(:votable)
#     puts "#{to_cache.length} karma points to be earned"
#     next unless to_cache.any?

#     # Creates a map of User -> Votes deserved
#     #                                      note not deleted
#     users_to_votes = to_cache.to_a.select { |s| s.votable }.inject({}) do |memo, vote|
#       memo[vote.votable.user_id] ||= 0 # The creator of the thing which was voted on
#       memo[vote.votable.user_id] += vote.value # probably only one anyway
#       memo
#     end

#     to_cache.update_all(karma_cached: true)

#     users_to_votes.each do |uid, inc|
#       u = User.find_by_id(uid)
#       return if !u
#       u.karma += inc
#       u.save
#     end
#   end
  
  # desc "Pick a random prize and winner"
  # task :all => [:prize, :winner]
  
  # def pick(model_class)
  #   model_class.find(:first, :order => 'RAND()')
  # end
# end