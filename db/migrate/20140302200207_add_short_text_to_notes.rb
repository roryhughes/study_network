class AddShortTextToNotes < ActiveRecord::Migration
  def up
    add_column :notes, :short_text, :text
    Note.all.each do |n|
      n.content_will_change!
      n.save
    end
  end
  def down
    remove_column :notes, :short_text
  end
end
