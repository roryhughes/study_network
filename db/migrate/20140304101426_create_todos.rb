class CreateTodos < ActiveRecord::Migration
  def change
    create_table :todos do |t|
      t.integer :goal_id
      t.boolean :done, default: false
      t.text :content

      t.timestamps
    end
  end
end
