class AddCachedToPageViewsAndViewCountOnNotes < ActiveRecord::Migration
  def change
    add_column :page_views, :count_cached, :boolean, default: false
    add_column :notes, :view_count, :integer, default: 0
    add_index :notes, :view_count
  end
end
