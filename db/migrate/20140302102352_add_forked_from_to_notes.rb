class AddForkedFromToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :forked_from, :integer
  end
end
