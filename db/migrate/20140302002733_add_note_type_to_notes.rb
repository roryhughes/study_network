class AddNoteTypeToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :note_type, :integer
    reversible do |dir|
      dir.up { Note.where(note_type: nil).update_all(note_type: 0) }
    end
  end
end
