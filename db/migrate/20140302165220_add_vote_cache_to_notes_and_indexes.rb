class AddVoteCacheToNotesAndIndexes < ActiveRecord::Migration
  def change
    add_column :notes, :vote_cache, :integer, default: 0

    add_column :votes, :cached, :boolean, default: false

    add_index :votes, [:votable_id, :votable_type]
    add_index :votes, :user_id
    add_index :votes, :cached

    add_index :notes, :user_id
    add_index :notes, :privacy
    add_index :notes, :created_at
    add_index :notes, :vote_cache
    add_index :notes, :forked_from
  end
end
