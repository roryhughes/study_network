class Search < ActiveRecord::Migration
  def up
    execute "create extension unaccent"
    execute "create index notes_title_search on notes using gin(to_tsvector('english', title))"
    execute "create index notes_content_search on notes using gin(to_tsvector('english', short_text))"
  end

  def down
    execute "drop extension unaccent"
    execute "drop index notes_title_search"
    execute "drop index notes_content_search"
  end
end
