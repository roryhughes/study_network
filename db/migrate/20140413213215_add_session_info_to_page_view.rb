class AddSessionInfoToPageView < ActiveRecord::Migration
  def change
    add_column :page_views, :session_id, :string
    add_column :page_views, :ip_address, :string
  end
end
