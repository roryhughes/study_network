class FirstAdminFeatures < ActiveRecord::Migration
  def change
    add_column :users, :admin, :datetime

    add_column :notes, :hidden, :datetime
    add_column :notes, :hidden_by, :integer
    add_column :notes, :hidden_reason, :text
    add_index :notes, :hidden

    add_column :notes, :featured, :datetime
    add_column :notes, :featured_by, :integer
    add_index :notes, :featured
  end
end
