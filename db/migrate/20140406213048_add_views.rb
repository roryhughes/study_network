class AddViews < ActiveRecord::Migration
  def change
    create_table :page_views do |t|
      t.integer :user_id
      t.references :viewable, polymorphic: true
      t.timestamps
    end
    add_index :page_views, [:viewable_id, :viewable_type]
    add_index :page_views, :user_id
  end
end
