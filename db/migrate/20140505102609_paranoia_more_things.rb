class ParanoiaMoreThings < ActiveRecord::Migration
  def change
    add_column :notifications, :deleted_at, :datetime
    add_index :notifications, :deleted_at

    add_column :notes, :deleted_at, :datetime
    add_index :notes, :deleted_at

    add_column :users, :deleted_at, :datetime
    add_index :users, :deleted_at
  end
end
