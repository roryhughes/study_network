class Notifications < ActiveRecord::Migration
  def up
    create_table :notifications do |t|
      t.integer :user_id
      t.text :content
      t.text :img_url
      t.datetime :read_at
      t.datetime :clicked_at

      t.text :link_path
      t.references :referencing, polymorphic: true # If there isn't a custom link, go to the object
      t.integer :act

      t.timestamps
    end

    add_index :notifications, :user_id
    add_index :notifications, :read_at
    add_index :notifications, [:referencing_type, :referencing_id]

    # Random
    add_index :goals, :user_id
  end

  def down
    drop_table :notifications
    remove_index :goals, :user_id
  end
end
