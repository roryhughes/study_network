class AddKarmaToUsers < ActiveRecord::Migration
  def change
    add_column :users, :karma, :integer, default: 0
    add_index :users, :karma
  end
end
