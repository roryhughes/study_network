class CreateGoals < ActiveRecord::Migration
  def change
    create_table :goals do |t|
      t.integer :user_id
      t.text :title
      t.datetime :due_at
      t.datetime :comp_at

      t.timestamps
    end
  end
end
