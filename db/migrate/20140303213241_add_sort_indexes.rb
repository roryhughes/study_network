class AddSortIndexes < ActiveRecord::Migration
  def change
    add_index :notes, [:created_at, :vote_cache] # Popular
    add_index :notes, :title # Title
  end
end
