class AddIndexesToTodos < ActiveRecord::Migration
  def change
    add_index :todos, :goal_id
  end
end
