class AddKarmaCachedToVotes < ActiveRecord::Migration
  def change
    add_column :votes, :karma_cached, :boolean, default: false
  end
end
