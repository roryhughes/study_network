# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140505102609) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "unaccent"

  create_table "goals", force: true do |t|
    t.integer  "user_id"
    t.text     "title"
    t.datetime "due_at"
    t.datetime "comp_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "goals", ["deleted_at"], name: "index_goals_on_deleted_at", using: :btree
  add_index "goals", ["user_id"], name: "index_goals_on_user_id", using: :btree

  create_table "notes", force: true do |t|
    t.string   "title"
    t.text     "content"
    t.integer  "privacy"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "note_type"
    t.integer  "forked_from"
    t.integer  "vote_cache",    default: 0
    t.text     "short_text"
    t.integer  "view_count",    default: 0
    t.datetime "hidden"
    t.integer  "hidden_by"
    t.text     "hidden_reason"
    t.datetime "featured"
    t.integer  "featured_by"
    t.datetime "deleted_at"
  end

  add_index "notes", ["created_at", "vote_cache"], name: "index_notes_on_created_at_and_vote_cache", using: :btree
  add_index "notes", ["created_at"], name: "index_notes_on_created_at", using: :btree
  add_index "notes", ["deleted_at"], name: "index_notes_on_deleted_at", using: :btree
  add_index "notes", ["featured"], name: "index_notes_on_featured", using: :btree
  add_index "notes", ["forked_from"], name: "index_notes_on_forked_from", using: :btree
  add_index "notes", ["hidden"], name: "index_notes_on_hidden", using: :btree
  add_index "notes", ["privacy"], name: "index_notes_on_privacy", using: :btree
  add_index "notes", ["title"], name: "index_notes_on_title", using: :btree
  add_index "notes", ["user_id"], name: "index_notes_on_user_id", using: :btree
  add_index "notes", ["view_count"], name: "index_notes_on_view_count", using: :btree
  add_index "notes", ["vote_cache"], name: "index_notes_on_vote_cache", using: :btree

  create_table "notifications", force: true do |t|
    t.integer  "user_id"
    t.text     "content"
    t.text     "img_url"
    t.datetime "read_at"
    t.datetime "clicked_at"
    t.text     "link_path"
    t.integer  "referencing_id"
    t.string   "referencing_type"
    t.integer  "act"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "notifications", ["deleted_at"], name: "index_notifications_on_deleted_at", using: :btree
  add_index "notifications", ["read_at"], name: "index_notifications_on_read_at", using: :btree
  add_index "notifications", ["referencing_type", "referencing_id"], name: "index_notifications_on_referencing_type_and_referencing_id", using: :btree
  add_index "notifications", ["user_id"], name: "index_notifications_on_user_id", using: :btree

  create_table "page_views", force: true do |t|
    t.integer  "user_id"
    t.integer  "viewable_id"
    t.string   "viewable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "count_cached",  default: false
    t.string   "session_id"
    t.string   "ip_address"
  end

  add_index "page_views", ["user_id"], name: "index_page_views_on_user_id", using: :btree
  add_index "page_views", ["viewable_id", "viewable_type"], name: "index_page_views_on_viewable_id_and_viewable_type", using: :btree

  create_table "todos", force: true do |t|
    t.integer  "goal_id"
    t.boolean  "done",       default: false
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "todos", ["deleted_at"], name: "index_todos_on_deleted_at", using: :btree
  add_index "todos", ["goal_id"], name: "index_todos_on_goal_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.integer  "karma",                  default: 0
    t.datetime "admin"
    t.datetime "deleted_at"
  end

  add_index "users", ["deleted_at"], name: "index_users_on_deleted_at", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["karma"], name: "index_users_on_karma", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "votes", force: true do |t|
    t.integer  "value"
    t.integer  "user_id"
    t.integer  "votable_id"
    t.string   "votable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "cached",       default: false
    t.boolean  "karma_cached", default: false
  end

  add_index "votes", ["cached"], name: "index_votes_on_cached", using: :btree
  add_index "votes", ["user_id"], name: "index_votes_on_user_id", using: :btree
  add_index "votes", ["votable_id", "votable_type"], name: "index_votes_on_votable_id_and_votable_type", using: :btree

end
