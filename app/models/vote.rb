class Vote < ActiveRecord::Base
  belongs_to :user
  belongs_to :votable, polymorphic: true

  validates :votable_id, uniqueness: {
    scope: [:user_id, :votable_type],
    message: 'You may only place one vote'
  }
  validates :votable_type, inclusion: { in: ['Note'] }

  # validates :value, inclusion: { in: (0..5), message: 'must be between 0 and 5' }
  validates :value, acceptance: { message: 'may only be 1', accept: 1 }

  scope :for_notes, -> { where(votable_type: 'Note') }
  scope :not_note_cached, -> { where(cached: false) }
  scope :not_karma_cached, -> { where(karma_cached: false) }

  def to_param
    votable
  end
end
