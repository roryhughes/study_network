class Goal < ActiveRecord::Base
  acts_as_paranoid

  has_many :todos, dependent: :destroy
  belongs_to :user

  validates :title, presence: true
  validates :title, length: { in: 1..200 }

  def todos_done
    todos.map(&:done).count(true)
  end
end
