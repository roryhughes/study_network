class PageView < ActiveRecord::Base
  belongs_to :user
  belongs_to :viewable, polymorphic: true

  # validates :viewable_id, uniqueness: {
  #   scope: [:user_id, :viewable_type],
  #   message: "One view per user"
  # }
  validates :viewable_type, inclusion: { in: ["Note"] }

  scope :for_notes, -> { where(viewable_type: "Note") }
  scope :not_note_cached, -> { where(count_cached: false) }
end