class Note < ActiveRecord::Base
  include ActionView::Helpers::DateHelper # For time ago in words

  include PgSearch
  pg_search_scope :search_by_title_short_text,
    against:  [:title, :short_text],
    using:    { tsearch: { dictionary: "english" } },
    ignoring: :accents

  URL_REGEXP = URI::Parser.new.make_regexp(['https'])

  acts_as_paranoid

  belongs_to :user
  has_many :votes, as: :votable, dependent: :destroy
  has_many :page_views, as: :viewable, dependent: :destroy

  # Allows for: note.forks and note.fork_ids
  has_many :forks, class_name: "Note", foreign_key: "forked_from"
  # Allows for: note.original_fork
  belongs_to :original_fork, class_name: "Note", foreign_key: "forked_from"

  belongs_to :hider, class_name: "User", foreign_key: "hidden_by"
  belongs_to :featurer, class_name: "User", foreign_key: "featured_by"

  PRINT_PRIVACIES = %w(Public Closed Private)
  enum privacy: [ :priv_public, :priv_closed, :priv_private ]
  enum note_type: [ :html_content, :url_frame ]

  scope :visible, -> { where(hidden: nil) }
  scope :featured, -> { where.not(featured: nil) }
  scope :featured_front, -> { priv_public.html_content.featured.order(created_at: :desc).limit(6) }

  validates :title, :user_id, :privacy, :content, presence: true
  validates :note_type, inclusion: { in: %w(html_content url_frame) }
  validates :privacy, inclusion: { in: %w(priv_public priv_closed priv_private),
    message: "%{value} is not a valid privacy level" }
  validates :title, length: { in: 1..200 }
  validates :content, length: { in: 1..100000,
    too_long: "too large, consider splitting into multiple notes or linking an external document" }
  validates :content, format: { with: URL_REGEXP,
    message: "must be valid web url" }, if: :url_frame?

  validates :hidden_reason, length: { in: 1..200 }, if: :hidden
  validate :fully_hidden

  before_save :set_short_text

  attr_readonly :note_type
  attr_readonly :forked_from

  # Custom validations
  def fully_hidden # All hidden must be set or none
    attrs = [hidden, hidden_by, hidden_reason]
    logger.info attrs
    errors.add(:base, "must have hidden, hidden reason and hider.") unless attrs.all? || attrs.none?
  end

  # Misc methods
  def editable
    created_at > 1.day.ago || priv_private? || hidden?
  end

  def fork_note
    self.class.new({
      note_type: note_type,
      content: content,
      title: "Copy of: " + title,
      privacy: privacy,
      forked_from: id
    })
  end

  # def get_forks_for_api
  #   Note.priv_public.where(forked_from: id).select(:id, :title)
  # end

  def time_for_display
    created_at > 7.days.ago ? time_ago_in_words(created_at) : created_at.strftime("%F")
  end

  def set_short_text
    # TODO Fix the... well all of this...
    return unless content_changed?
    if html_content?
      # TODO look actual errors
      begin
        self.short_text = Nokogiri::HTML(content).text[ 0, 600 ].gsub(/\W+/, " ")
      rescue
        self.short_text = self.title
      end
    else
      self.short_text = self.title
    end
  end

  def view_from(user, sess, ip)
    Thread.new do
      ActiveRecord::Base.connection_pool.with_connection do |conn|
        p = page_views.create(
          session_id: sess,
          ip_address: ip,
          user: user
        )
      end
    end
  end

  def privacy_string
    PRINT_PRIVACIES[self['privacy']]
  end

  def feature!(u)
    self.featured = Time.now
    self.featurer = u
  end

  def unfeature!
    self.featured = nil
    self.featurer = nil
  end

  def hide!(u, reason)
    self.hidden = Time.now
    self.hider = u
    self.hidden_reason = reason
  end

  def unhide!
    self.hidden_reason = nil
    self.hider = nil
    self.hidden = nil
  end

  def viewable_by?(viewer)
    return true if viewer && viewer.id == user_id # Always if creator
    # Must NOT be private and (NOT be hidden Or otherwise viewer must be an admin)
    !priv_private? && (!hidden? || (viewer && viewer.admin?))
  end

  # Class methods

  def self.viewable_publicly(viewer)
    q = priv_public
    viewer.admin? ? q : q.visible
  end

  ###########
  # Workers #
  ###########
  def self.count_page_views
    # Get the note ids of page_views which aren't cached already,
    pv_note_ids = PageView.for_notes
      .not_note_cached
      .pluck(:viewable_id)

    return job_msg("0") unless pv_note_ids.size > 0

    # Update the view_count of each note in the list to the
    # total length of its views.
    note_count = Note.where(id: pv_note_ids.uniq)
      .includes(:page_views)
      .each { |note| note.update(view_count: note.page_views.size) }.size

    # Mark page_views as cached
    PageView.not_note_cached.update_all(count_cached: true)

    job_msg("#{pv_note_ids.size} new views on #{note_count} unique notes")
  end

  def self.cache_votes
    # Get non-cached vote ids
    vote_note_ids = Vote.for_notes
      .not_note_cached
      .pluck(:votable_id)

    return job_msg("0") unless vote_note_ids.size > 0

    note_count = Note.where(id: vote_note_ids.uniq)
      .includes(:votes)
      .each { |note| note.update(vote_cache: note.votes.size) }.size

    Vote.not_note_cached.update_all(cached: true)

    job_msg("#{vote_note_ids.size} new votes on #{note_count} unique notes")
  end
end
