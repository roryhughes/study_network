class Notification < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  include ActionView::Helpers::DateHelper # For time ago in words

  acts_as_paranoid
  
  belongs_to :user
  belongs_to :referencing, polymorphic: true

  enum act: [
    :fork_note,
    :noteworthy,
    :group_invite,
    :goal_complete,
    :comment_on_note,
    :from_team,
    :note_featured,
    :note_hidden
  ]

  validates :user_id, :content, presence: true

  scope :for_list, -> {
    # where("notifications.read_at > ? OR notifications.read_at IS\ NULL", 1.day.ago)
    order(created_at: :desc)
    .limit(8)
  }
  scope :unread, -> { where("read_at IS NULL") }

  def icon_class
    # Corresponding to the act enum
    [
      "code-fork",         # :fork_note
      "star",              # :noteworthy
      "users",             # :group_invite
      "clock-o",           # :goal_complete
      "comment-o",         # :comment_on_note
      "smile-o",           # :from_team
      "certificate",       # :note_featured
      "exclamation-circle" # :note_hidden
    ][self['act']]
  end

  def object_to_link
    return unless referencing_id
    if referencing.class == Vote
      referencing.votable
    else
      referencing
    end
  end

  def path
    return link_path if link_path
    o = object_to_link
    o && url_for([o, {:only_path => true}])
  end

  def should_be_clickable?
    !!(link_path || referencing_id)
  end

  def no_path_error
    if referencing_id # || link_path -> don't think there is any point
      destroy
      {
        alert: "For some reason this notification is referencing "\
            "a #{referencing_type || 'thing'} which doesn't exist "\
            "any more. (deleting notification)"
      }
    else
      { notice: "This notification doesn't appear to reference anything... :/" }
    end
  end

  def click!
    clicked_at ? false : (self.clicked_at = Time.now)
  end

  def time_for_display
    created_at > 7.days.ago ? time_ago_in_words(created_at) : created_at.strftime("%F")
  end

  def self.read!
    where(read_at: nil).update_all(read_at: Time.now)
  end
end