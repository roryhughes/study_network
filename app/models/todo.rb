class Todo < ActiveRecord::Base
  acts_as_paranoid
  
  belongs_to :goal
  delegate :user, :to => :goal, :allow_nil => true

  validates :done, inclusion: { in: [false, true] }
  validates :content, length: { in: 1..300 }
  
end
