class User < ActiveRecord::Base
  # include Rails.application.routes.url_helpers

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  acts_as_paranoid

  has_many :votes, dependent: :destroy

  has_many :notes, dependent: :destroy
  has_many :recieved_votes, through: :notes, source: :votes
  has_many :noteworthies, through: :votes, source: :votable, source_type: "Note"
  has_many :notifications, dependent: :destroy

  has_many :goals, dependent: :destroy

  validates :name, presence: true

  after_create :add_welcome_notification

  def medals
    m = []
    m.push "bronze" if karma >= 2
    m.push "silver" if karma >= 10
    m.push "gold"   if karma >= 30
    m
  end

  def total_karma_using_cache
    notes.select(:vote_cache).reduce(0) { |tot, n| tot += n.vote_cache }
  end

  def total_karma_vote_count
    recieved_votes.size
  end

  def admin!
    self.admin = Time.now
  end

  #Notifications
  def add_notification(ref, act, content, url: nil)
    Thread.new do
      ActiveRecord::Base.connection_pool.with_connection do |conn|
        n = notifications.new(content: content, act: act)
        n.link_path = url
        if ref.is_a?(String)
          n.link_path ||= ref
        else
          n.referencing = ref
          # n.link_path = url_for([n.object_to_link, {:only_path => true}])
        end
        n.save!
      end
    end
  end

  def n_count
    notifications.unread.count
  end

  def add_welcome_notification
    add_notification(
      "/n/new",
      :from_team,
      "Welcome to StudyNet #{name}! Click here to learn more about the site."
    )
  end

  def self.notify_existing_users
    all.each do |u|
      u.add_notification(
        "/goals",
        :from_team,
        "Hey #{u.name}! New: Notifications keep you up to date with activity on StudyNet. "\
          "Have you used Goals yet? Click here to get started!"
      )
    end
  end

  ###########
  # Workers #
  ###########
  def self.cache_karma(opts = {})
    #smelly

    user_ids = []
    if opts[:all]
      user_ids = User.pluck(:id)
    elsif opts[:users]
      user_ids = opts[:users]
    else
      # Find all new votes, get their notes, and get the user_ids of the notes
      user_ids = Vote.for_notes
        .not_karma_cached
        .includes(:votable)
        .map(&:votable).map(&:user_id).uniq
    end

    return job_msg("0") unless user_ids.size > 0

    users_to_notes = Note.where(user_id: user_ids).group_by(&:user_id)
    users_to_karma = Hash[users_to_notes.map do |k, v|
      [k, v.reduce(0) { |tot, n| tot += n.vote_cache }]
    end]

    users_to_karma.each do |uid, k|
      where(id: uid).update_all(karma: k)
    end

    vote_count = Vote.not_karma_cached.update_all(karma_cached: true)

    job_msg("#{vote_count} new votes deserved by #{user_ids.size} unique users")
  end
end
