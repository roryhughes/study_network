module ApplicationHelper
  def link_to_if_with_block condition, options, html_options={}, &block
    if condition
      link_to options, html_options, &block
    else
      content_tag :a, html_options, &block
    end
  end

  def i(n, title: nil)
    return "" unless n
    content_tag(:i, nil, class: "fa fa-#{n}", title: title)
  end

  def nav_link(*args, &block)
    if current_page?(args[block_given? ? 0 : 1])
      if args.last.is_a? Hash
        args.last[:class] ||= ''
        args.last[:class] += ' current-page'
      else
        args.push({ class: "current-page" })
      end
    end
    link_to *args, &block
  end

  def current_filter(*args)
    if @sort == (args[0])
      if args.last.is_a? Hash
        args.last[:class] ||= ''
        args.last[:class] += ' current-filter'
      else
        args.push({ class: "current-filter" })
      end
    end
    link_to *args
  end

  def medals_content(medals)
    medals.each do |med|
      m = content_tag(:span, class: "fa-stack medal", title: "#{med.capitalize} Medal") do
        i("circle fa-stack-2x #{med}") +
        i("star fa-stack-1x")
      end
      concat m
    end
    return nil
  end

  def close_modal
    button_tag "&times;".html_safe, data: { dismiss: "modal" }, class: "close" if @is_modal
  end

  def note_hidden
    content_tag(:span, class: "label label-warning", title: "Note hidden by admin.") do
      i("exclamation-circle") + " Hidden"
    end
  end

  def note_featured
    content_tag(:span, class: "fa-stack featured-icon", title: "Featured Note") do
      i("certificate fa-stack-2x") +
      content_tag(:span, "S", class: "fa-stack-1x")
    end
  end

  def noteworthies(n)
    content_tag(:span, class: "badge nw") do
      "#{n.to_s} #{i("star")}".html_safe
    end
  end

  # Legacy modal system
  # def smodal(name)
  #   o = render file: "modal/#{name}"
  #   raw(o.sub "&times;", "")
  # end
end
