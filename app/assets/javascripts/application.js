//= require jqujs-rails

//= require turbolinks

//= require main
//= require notes
//= require goals

//= require_self

window.log = function() {
  log.history = log.history || [];
  log.history.push(arguments);
  if (this.console) {
    var toLog = Array.prototype.slice.call(arguments);
    return console.log(toLog.length > 1 ? toLog : toLog[0]);
  }
};


var isTouchDevice = 'ontouchstart' in document.documentElement;
if (isTouchDevice) {
  $("html").addClass("touch-device")
}