# $(".summernote-main").destroy();
sn.loadSummerNoteEditor = ->
  $(".summernote-main").summernote
    height: 400
    onImageUpload: (files, editor, welEditable) ->
      sendFile files[0], editor, welEditable

sendFile = (file, editor, welEditable) ->
  fd = new FormData() # https://hacks.mozilla.org/2011/01/how-to-develop-a-html5-image-uploader/
  fd.append "image", file
  fd.append "key", "b7ea18a4ecbda8e92203fa4968d10660"
  xhr = new XMLHttpRequest()
  xhr.open "POST", "http://api.imgur.com/2/upload.json"
  xhr.onload = ->
    try
      res = JSON.parse(xhr.responseText)
    catch e
      return uploadError()
    imageUrl = res["upload"]["links"]["original"]
    editor.insertImage welEditable, imageUrl

  xhr.onerror = uploadError
  xhr.send fd

uploadError = ->
  alert "Error uploading image, try again soon..."
