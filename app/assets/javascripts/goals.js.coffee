class Todo
  # KEY_NAMES: ["id", "content", "done"]
  constructor: (apiObj) ->
    that = this
    # @KEY_NAMES.forEach (k) ->
    #   that[k] = apiObj[k]
    that.id = apiObj.id
    that.done = apiObj.done
    that.content = sn.linkify(apiObj.content)

class Goal
  KEY_NAMES: ["id", "title", "due_at", "comp_at", "updated_at", "created_at"]

  constructor: (apiObj) ->
    @todos = {}
    that = this
    @KEY_NAMES.forEach (k) ->
      that[k] = apiObj[k]
    if apiObj.todos && apiObj.todos.length
      apiObj.todos.forEach (t) -> that.todos[t.id] = new Todo t

  doneList: ->
    Object.filterKV @todos, (k, v) -> v.done

  notDoneList: ->
    Object.filterKV @todos, (k, v) -> !v.done

  fullCount: ->
    Object.keys(@todos).length

  doneCount: ->
    Object.keys(@doneList()).length
    # Old, not sure if faster
    # Object.keys(@todos).reduce (mem, cur) ->
    #   mem += 1 if that.todos[cur].done
    #   mem
    # , 0

  percentage: ->
    len = @fullCount()
    return 0 unless len
    Math.floor(100 * (@doneCount() / len))

  doneStr: ->
    len = @fullCount()
    return "<i class='fa fa-minus'></i>" unless len
    done = @doneCount()
    return "<span class='text-successer'>#{ len }
      <i class='fa fa-check'></i>" if done is len
    "#{ done }/#{ len } done"

  addTodo: (content) ->
    that = this
    sn.l true
    $.ajax(
      type: "POST"
      url: "/goals/#{that.id}/t.json"
      contentType: "application/json"
      processData: false
      data: JSON.stringify {todo: {content: content}}
    ).done((todo) ->
      that.todos[todo.id] = new Todo(todo)
      that.renderTodos()
    ).fail(@errorsBSAlert.bind(this)).always -> sn.l false

  updateTodo: (id, done, content) ->
    return unless @todos[id]
    that = this
    sn.l true
    $.ajax(
      type: "PUT"
      url: "/goals/#{that.id}/#{id}"
      contentType: "application/json"
      processData: false
      data: JSON.stringify {todo: {content: content, done: done}}
    ).done((todo) ->
      that.todos[todo.id] = new Todo(todo)
      that.renderTodos()
    ).fail(@errorsBSAlert.bind(this)).always -> sn.l false

  removeTodo: (id) ->
    return unless @todos[id]
    that = this
    sn.l true
    $.ajax(
      type: "DELETE"
      url: "/goals/#{that.id}/#{id}.json"
      contentType: "application/json"
      processData: false
    ).done((todo) ->
      delete that.todos[id]
      that.renderTodos()
    ).fail(@errorsBSAlert.bind(this)).always -> sn.l false

  renderHeader: ->
    @dom.find(".panel-heading").html(sn.hbT.goalHeader(this))

  renderTodos: ->
    @renderHeader()

    @dom.find(".todo-list.undone-todos") # Not done
      .html(sn.hbT.goalTodoList(todosLoaded: @notDoneList(), notDone: true))
    @dom.find(".todo-list.done-todos")
      .html(sn.hbT.goalTodoList(todosLoaded: @doneList()))

  errorsBSAlert: (res) ->
    errorString = res.responseJSON["errors"].join("</li><li>")
    @dom.find(".todo-list.undone-todos").prepend("
        <div class=\"alert alert-danger smalert\"><ul><li>
          #{errorString}
        </li></ul></div>")

GoalsApp =
  goalStore: {}

  cacheElements: ->
    @goalsDom = $ "#goals-app"
    @el =
      gList: @goalsDom.find ".goals-list"
      newGoal: @goalsDom.find "#add-goal"

  bindGeneral: ->
    @el.newGoal.on "submit", @createGoal.bind(this)
    @el.newGoal.find(".reload-goals").click @reloadGoals.bind(this)
    # @goalsDom.on "click", ".g-destroy", @destroyGoal.bind(this)

  bindGoal: (g) ->
    g.dom.find(".g-destroy").click @destroyGoal.bind(g)
    g.dom.find(".g-edit").click @editGoal.bind(g)
    g.dom.find(".g-add-todo").submit @addTodo.bind(g)

    g.dom.on "click", ".g-t-destroy", @removeTodo.bind(g)
    g.dom.on "change", ".g-t-checkbox", @updateTodoDone.bind(g)

  renderGoalPanel: (g) ->
    gEl = $("#goal-#{g.id}")
    if !gEl.length
      @el.gList.prepend(
        "<div id=\'goal-#{g.id}\' class=\"goal panel panel-default\"></div>"
      )
      gEl = $("#goal-#{g.id}")

    gEl.html(sn.hbT.goal(g))
    g.dom = gEl
    g.renderTodos()
    @bindGoal(g)

  renderAll: ->
    that = this
    $.each that.goalStore, (i, g) ->
      that.renderGoalPanel g
    unless Object.keys(@goalStore).length
      $(".no-goals").removeClass("hidden")

  loadAllGoals: (cb) ->
    that = this
    $.get("/goals.json").done (goalsList) ->
      goalsList.forEach (g) ->
        that.goalStore[g.id] = new Goal g
      cb()

  reloadGoals: (e) ->
    that = this
    sn.l yes # CoffeeScript madness
    @goalStore = {}
    @loadAllGoals ->
      that.el.gList.html("")
      that.renderAll()
      sn.l no

  # Operations
  createGoal: (e) ->
    e.preventDefault()
    that = this
    inputEl = @goalsDom.find("input")
    val = inputEl.val()
    return unless val.length
    inputEl.val("")
    postData = {title: val}

    sn.l true
    $.ajax(
      type: "POST"
      url: "/goals.json"
      contentType: "application/json"
      processData: false
      data: JSON.stringify postData
    ).done((goal) ->
      that.goalStore[goal.id] = new Goal goal
      that.renderGoalPanel(that.goalStore[goal.id])
      if Object.keys(that.goalStore).length
        $(".no-goals").addClass("hidden")
    ).fail(@goalErrorsAlert).always -> sn.l false

  editGoal: (e) ->
    e.preventDefault()
    that = this
    originalTitle = @title

    @dom.find(".goal-title").html "
      <form class=\"form-inline edit-title\" role=\"form\">
        <input class=\'form-control input-sm\' placeholder=\'Enter a new title...\'>
      </form>
    "
    formEl = @dom.find(".edit-title")
    formEl.find("input")
          .val(originalTitle)
          .focus()
          .keyup (e) ->
            that.renderHeader() if e.which is 27

    formEl.submit (e) ->
      e.preventDefault()
      newVal = formEl.find("input").val()
      if !newVal.length or newVal is originalTitle
        that.renderHeader()
        return
      sn.l true
      $.ajax(
        type: "PUT"
        url: "/goals/#{that.id}"
        contentType: "application/json"
        processData: false
        data: JSON.stringify {title: newVal}
      ).done((goal) ->
        that.title = goal.title
        that.renderHeader()
        # Old
        # # Weird here because overwriting goal object
        # id = that.id
        # g = new Goal goal
        # GoalsApp.goalStore[id] = g
        # GoalsApp.renderGoalPanel g
      ).fail(GoalsApp.goalErrorsAlert).always -> sn.l false

  destroyGoal: (e) ->
    e.preventDefault()
    return unless confirm "Are you sure you want to delete \'#{@title}\'?"
    sn.l true
    t = this
    $.ajax(
      type: "DELETE"
      url: "/goals/#{this.id}.json"
      contentType: "application/json"
      processData: false
    ).done((goal) ->
      delete GoalsApp.goalStore[t.id]
      t.dom.remove()
    ).fail(-> alert "Error deleting, try refreshing page.").always -> sn.l false

  goalErrorsAlert: (res) ->
    alert "Server error:\n\n#{res.responseJSON["errors"].join(", ")}"

  # TODOs
  todoId: (el) ->
    $(el).closest(".todo").attr("id")

  addTodo: (e) ->
    e.preventDefault()
    inputEl = @dom.find(".g-add-todo input")
    content = inputEl.val()
    return unless content.length
    this.addTodo(content)
    inputEl.val("")

  updateTodoDone: (e) ->
    e.preventDefault()
    id = GoalsApp.todoId(e.target)
    checked = $(e.target).is(":checked")
    @updateTodo(id, checked, undefined)

  removeTodo: (e) ->
    e.preventDefault()
    id = GoalsApp.todoId(e.target)
    @removeTodo(id)

  #INIT
  init: ->
    that = this
    @cacheElements()
    @bindGeneral()

    @loadAllGoals ->
      that.renderAll()
      sn.l false

sn.loadGoals = ->
  GoalsApp.init()
