Object.filterKV = (obj, fn) ->
  result = {}
  for k, v of obj
    if fn(k, v)
      result[k] = v
  result

sn.thingsLoading = 0
sn.l = (x) ->
  if x
    sn.thingsLoading += 1
  else
    sn.thingsLoading -= 1
  sn.thingsLoading = 0 if sn.thingsLoading < 0

  if sn.thingsLoading
    $(".loading").removeClass "ready"
  else
    $(".loading").addClass "ready"

sn.lastChangeMs = 0
sn.now = -> new Date().getTime()
sn.pageActive = -> sn.lastChangeMs = sn.now()
sn.activeAgo = -> sn.now() - sn.lastChangeMs

sn.linkify = (inputText) ->
  #URLs starting with http://, https://, or ftp://
  replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim
  replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>')
  #URLs starting with "www." (without // before it, or it'd re-link the ones done above).
  replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim
  replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>')

sn.camelCase = (s) ->
  s.replace /(\-[a-z])/g, (d) -> d.toUpperCase().replace('-', '')

compileHbTmpls = ->
  $("script[type='text/x-handlebars-template']").each ->
    el = $(@)
    jsTitle = sn.camelCase(el.attr("id"))
    sn.hbT[jsTitle] = Handlebars.compile(el.html())

sn.user = false

# MODALS
mainModal = undefined
  
loadRemoteModal = (path, msg) ->
  # Start loading indicator
  sn.l true
  $.get(path, {m: '1'}).done((page) ->
    mainModal.html page
    mainModal.modal "show"
    mainModal.find(".modal-header").after("<div class=\"alert alert-warning smalert text-center\">#{msg}</div>") if msg
    sn.l false
  ).fail ->
    alert "Error loading, please refresh page"
    sn.l false

remoteModal = (path, msg) ->
  if mainModal.hasClass 'in'
    mainModal.modal "hide"
    mainModal.one 'hidden.bs.modal', (e) ->
      loadRemoteModal(path, msg)
  else
    loadRemoteModal(path, msg)

$(document).on "click", ".remote-modal", (e) ->
  e.preventDefault()
  path = $(this).data("modalpath") || $(this)[0].pathname
  remoteModal(path)

# Page load
$(document).ready ->
  sn.l true
  compileHbTmpls()
  Notifications.getStaticCount() if sn.user

$(document).on "page:fetch", ->
  sn.l true

$(document).on "page:change", ->
  sn.l false
  sn.user = $(".user-nav").length > 0
  if sn.user
    userPageChange()
  else
    #MBS: must be signed in
    $(".mbs").click (e) ->
      e.preventDefault()
      remoteModal("login", "You must be signed in to do that!")
  mainModal = $("#main-modal").modal(show: false)
  $(".ttp").tooltip
    placement: "bottom"
    container: "body"


userPageChange = ->
  sn.pageActive()
  Notifications.bindEl()
  Notifications.updateBtn()
  Notifications.renderList() if Notifications.list.length
  Notifications.unreadCheckIn(6000)

  #NOTEWORTHY BUTTON
  setNwTtp = ->
    return if $(".noteworthy").hasClass("nw-example")
    $(".noteworthy").tooltip
      placement: "bottom"
      title: sn.noteworthy[(if $(".noteworthy").hasClass("is-nw") then "is" else "not")]
      container: "body"

  setNwTtp()
  $(".noteworthy.not-nw").click ->
    btn = $(this)
    noteId = btn.data("note-id")
    return unless noteId
    btn.attr "data-note-id", null
    btn.data "note-id", null
    $.post("/v/note/" + noteId + "/1").done( ->
      btn.removeClass("not-nw").addClass "is-nw"
      currentCount = parseInt(btn.find(".vote-count").html())
      btn.find(".vote-count").html currentCount + 1
      btn.tooltip "destroy"
      setNwTtp()
    ).fail ->
      btn.html "<span class=\"text-danger\">
                <i class=\"fa fa-times-circle-o\"></i>
                <strong>Error</strong>
                <small>Refresh and try again</small></span>"

Notifications =
  list: []
  unreadCount: 0
  lastListGet: 0
  el: null
  unreadTimeout: null

  bindEl: ->
    that = this
    @el = $("#notification-dropdown")

    @el.on "show.bs.dropdown", ->
      if ((sn.now()) - that.lastListGet) > 6000 || that.unreadInList()
        that.showLoading()
        that.getList(that.renderList.bind(that))

    @el.on "hide.bs.dropdown", ->
      that.updateBtn()

  getList: (cb) ->
    that = this
    @lastListGet = sn.now()
    $.getJSON("/notifications.json").done((list) ->
      that.list = list
      cb()
    ).fail( ->
      # alert("notifications error");
    )

  getCount: (cb) ->
    that = this
    $.getJSON("/notifications/count.json").done((c) ->
      that.unreadCount = c.count
      cb()
    ).fail( ->
      # alert("notifications error");
    )

  getStaticCount: ->
    # Can't use @el here because this function is being called in document
    # ready and bindEl hasn't happened yet
    strCount = $("#notification-dropdown .unread").html()
    numCount = parseInt(strCount)
    # Better than checking is ""
    @unreadCount = if isNaN(numCount) then 0 else numCount

  unreadCheckIn: (ms) ->
    @unreadTimeout && clearTimeout(@unreadTimeout)
    that = this
    min = 1200
    max = 45000
    ms = min if !ms || (ms < min)
    @unreadTimeout = setTimeout( ->
      that.getCount ->
        that.updateBtn()
        newMs = 5000 + (sn.activeAgo() / 3)
        newMs = max if newMs > max
        that.unreadCheckIn(newMs)
    , ms)

  unreadInList: ->
    return 0 unless @list.length
    @list.map((n) ->
      !n.read_at
    ).reduce((m, n) ->
      if n then m + 1 else m
    , 0)

  extraUnread: ->
    @unreadCount - @unreadInList()

  renderList: ->
    @el.find(".notification-list").html(sn.hbT.notificationList(this))

  showLoading: ->
    @el.find(".notification-list").html("<li class=\"text-muted text-center\">Loading...</li>")

  updateBtn: ->
    unread = @el.find(".unread")
    btn = @el.find(".notification-btn")
    c = @extraUnread()
    c = 0 if c < 0 # TODO fix this, went -1 for some reason on signup
    if c
      unread.html(c)
      btn.removeClass("none").addClass("some")
    else
      unread.html("")
      btn.removeClass("some").addClass("none")

  len: ->
    @list.length

Handlebars.registerHelper "nLink", (clickable, id) ->
  if clickable then "href=\"/notifications/#{id}\"" else ""

Handlebars.registerHelper 'f', (fname, params...) ->
  o = if this[fname] then this[fname](params...) else ""
  new Handlebars.SafeString(o)

Handlebars.registerHelper 'fEq', (fname, eq, options) ->
  o = if this[fname] then this[fname]() else null
  if(o == eq)
    return options.fn(this);
  return options.inverse(this);

Handlebars.registerHelper 'checked', (b) ->
  if b then "checked" else ""

Handlebars.registerHelper 'eachFnObj', (fname, options) ->
  o = if this[fname] then this[fname]() else {}
  Handlebars.helpers.each(o, options)

Handlebars.registerHelper 's', (text) ->
  new Handlebars.SafeString text

Handlebars.registerHelper 'sIf', (check, cname) ->
  if check then cname else ""

Handlebars.registerHelper 'sUn', (check, cname) ->
  if !check then cname else ""