json.array! @goals do |g|
  json.(g, :id, :title, :due_at, :comp_at, :created_at, :updated_at)
  json.todos g.todos, :id, :content, :done
end