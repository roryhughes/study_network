json.array! @notifications do |n|
  json.(n,
    :id,
    :content,
    :created_at,
    :read_at,
    :clicked_at,
    :act
  )
  json.icon_class n.icon_class
  json.time_str n.time_for_display
  json.clickable n.should_be_clickable?
end