class VotesController < ApplicationController
  before_action :authenticate_user!

  def create
    model_str = params[:model]
    id = params[:id].to_i

    # find model
    return vote_error("\'#{model_str}\' not votable") unless ["note"].include? model_str
    unless id > 0 && (votable = model_str.classify.constantize.find_by_id(id))
      return vote_error("cannot find \'#{model_str}\' with id #{params[:id]}")
    end

    value = params[:value]
    vote = votable.votes.build(
      value: value.to_i
    )
    vote.user = current_user

    # Can't vote on your own stuff
    return vote_error("You may not vote on your own #{model_str}") if votable.user_id == vote.user_id

    if vote.save
      render json: vote
      vote.votable.user.add_notification(
        vote,
        :noteworthy,
        "#{current_user.name} noteworthied your note: '#{vote.votable.title.truncate(20)}'"
      )
    else
      vote_error(vote.errors.full_messages)
    end
  end

private
  def vote_error(e)
    e = [e] if e.is_a? String
    render json: { errors: e }, status: 400
  end
end