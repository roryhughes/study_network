class GoalsController < ApplicationController
  before_action :find_goal, except: [:index, :info, :create]
  before_action :must_be_creator, except: [:index, :info, :create]
  before_action :find_todo, only: [:update_todo, :destroy_todo]

  def index
    @title = "Goals"
    respond_to do |f|
      f.html
      f.json do
        @goals = current_user.goals.order(id: :desc).includes(:todos).order('todos.id DESC')
      end
    end
  end

  def info
    render 'info', layout: @layout
  end

  def show
  end

  def create
    @goal = current_user.goals.new(goal_params)
    if @goal.save
      render json: @goal
    else
      render json: errors(@goal.errors.full_messages), status: 400
    end
  end

  def update
    if @goal.update_attributes(goal_params)
      render json: @goal
    else
      render json: errors(@goal.errors.full_messages), status: 400
    end
  end

  def destroy
    @goal.destroy
    render json: { action: "deleted" }
  end

  def create_todo
    todo = @goal.todos.new(todo_params)
    if todo.save
      render json: todo
    else
      render json: errors(todo.errors.full_messages), status: 400
    end
  end

  def update_todo
    if @todo.update_attributes(todo_params)
      render json: @todo
    else
      render json: errors(@todo.errors.full_messages), status: 400
    end
  end

  def destroy_todo
    @todo.destroy
    render json: { action: "deleted" }
  end

private
  def find_goal
    @goal = Goal.find_by_id(params[:id])
    render json: errors("Cannot find goal.") unless @goal
  end

  def find_todo
    @todo = @goal.todos.find_by_id(params[:tid])
  end

  def must_be_creator
    render json: errors("Cannot find goal."), status: 400 unless current_user.id == @goal.user_id
  end

  def todo_params
    params.require(:todo).permit(:content, :done)
  end

  def goal_params
    puts params
    params.require(:goal).permit(:title, :due_at)
  end

  def errors(e)
    { errors: (e.is_a? Array) ? e : [e] }
  end
end
