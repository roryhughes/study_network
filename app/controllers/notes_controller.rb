class NotesController < ApplicationController
  skip_before_action :authenticate_user!, only: [:show]
  before_action :set_note,                only: [:show, :edit, :update, :destroy, :forks]
  before_action :must_be_creator,         only: [:edit, :update, :destroy]
  before_action :must_be_editable,        only: [:edit, :update]
  before_action :must_be_viewable,        only: [:show, :forks]

  def index
    @title = "Explore Notes"

    @sort_types = ["popular", "views", "latest", "oldest", "title"]
    if params[:sort]
      if (@sort_types.include? params[:sort])
        @sort = params[:sort]
      else
        flash[:alert] = "Invalid sort type."
      end
    end
    @sort ||= "popular"

    q = Note.viewable_publicly(current_user) # <= very important

    @search_q = params[:q]
    if @search_q && @search_q.length > 0
      if @search_q.length < 100
        q = q.search_by_title_short_text(@search_q)
      else
        flash[:alert] = "Search query must be less than 100 characters"
      end
    end

    case @sort
    when "views"
      q = q.order(view_count: :desc)
    when "latest"
      q = q.order(created_at: :desc)
    when "oldest"
      q = q.order(created_at: :asc)
    when "title"
      q = q.order(:title)
    else
      # popular
      q = q.order(vote_cache: :desc, created_at: :desc)
    end

    @notes = q.sexcept(:content).page(params[:page]).per(10).includes(:user)
  end

  def new
    @title = "Create note"

    forking_id = params[:forking].to_i
    if forking_id > 0 && (forking = Note.find_by_id(params[:forking]))
      @note = forking.fork_note
      return render 'new'
    end
    # If it reaches this point with an attemted fork:
    flash[:notice] = "Could not fork note with id \'#{params[:forking]}\'" if params[:forking]
    type = params[:type]
    if type && Note.note_types.include?(type)
      @note = Note.new(note_type: type)
      render 'new'
    else
      flash[:alert] = "Invalid note type" if type
      render 'choose_type'
    end
  end

  def create
    @note = current_user.notes.new(note_params)

    @note.valid? # Putting this in the else section gets captcha error overwritten :(
    if verify_recaptcha(model: @note, message: "Incorrect Captcha.") && @note.save
      redirect_to @note, notice: 'Note saved'
      if @note.original_fork && (current_user.id != @note.original_fork.user_id)
        @note.original_fork.user.add_notification(
          @note,
          :fork_note,
          "#{current_user.name} forked your note: '#{@note.original_fork.title.truncate(30)}'"
        )
      end
    else
      render action: 'new'
    end
  end

  # For one particular note
  def show
    @title = view_context.truncate(@note.title, length: 20)

    @is_creator = is_creator?
    unless @is_creator
      @note.view_from(current_user, session[:session_id], request.remote_ip)
    end
    @disqus_id = "note-#{@note.id}"
    @disqus_title = view_context.truncate(@note.title, length: 60)
    if @note.forked_from
      @original_fork = @note.original_fork
    end
    @user_has_voted = current_user && @note.votes.where(user_id: current_user.id).first
  end

  def edit
    @title = "Edit #{view_context.truncate(@note.title, length: 20)}"
  end

  def update
    if @note.update(note_params)
      redirect_to @note, notice: 'Note updated'
    else
      render action: 'edit'
    end
  end

  def destroy
    @note.destroy
    redirect_to notes_url, notice: "Note deleted"
  end

  def forks
    @notes = @note.forks.viewable_publicly(current_user)
                  .order(created_at: :desc).page(params[:page]).per(10).load
    respond_to do |f|
      f.html do    
        render 'forks', layout: @layout
      end
      # f.json do
      #   render json: @notes
      # end
    end
  end

  NOTE_ERROR = "Error accessing note."

  private
    def set_note
      @note = Note.find_by_id(params[:id])
      return redirect_to notes_path, alert: NOTE_ERROR unless @note
    end

    def note_params
      params.require(:note).permit(:title, :content, :privacy, :note_type, :forked_from)
    end

    # Permissions
    def is_creator?
      current_user && @note.user_id == current_user.id
    end

    def must_be_creator
      return redirect_to @note, alert: NOTE_ERROR unless is_creator?
    end

    def must_be_editable
      redirect_to @note, alert: "You may not edit a public note more than 24 hours after creation." unless @note.editable
    end

    def must_be_viewable
      return redirect_to notes_path, alert: NOTE_ERROR unless @note.viewable_by?(current_user)
    end
end
