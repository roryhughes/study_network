class RegistrationsController < Devise::RegistrationsController
  prepend_before_filter :authenticate_scope!, only: [:edit, :update, :destroy, :delete_account, :show]

  def new
    build_resource({})
    respond_with self.resource, layout: @layout
  end

  def create
    flash.delete :recaptcha_error
    if verify_recaptcha
      super
    else
      build_resource(sign_up_params)
      resource.valid?
      resource.errors.add(:base, "Incorrect captcha.")
      clean_up_passwords(resource)
      render :new
    end
  end

  def show
    # Redirect to dashboard if user is current user
    return redirect_to root_path if current_user && (current_user.id == params[:id].to_i)

    @user = User.find_by_id(params[:id])
    return redirect_to root_path, alert: "No such user id: #{params[:id]}" unless @user

    @notes = @user.notes.viewable_publicly(current_user).order(created_at: :desc).page(params[:page]).per(10).load
    @medals = @user.medals

    @title = "User: #{view_context.truncate(@user.name, length: 20)}"
  end

  def delete_account
    modal_only
    render 'delete_account', layout: @layout
  end

  def destroy
    unless resource.valid_password?(params[:current_password])
      return redirect_to edit_user_registration_path, alert: "Password incorrect. Account NOT deleted."
    end
    super
  end

  def after_update_path_for(resource)
    signed_in_root_path(resource)
    #edit_user_registration_path
  end
end