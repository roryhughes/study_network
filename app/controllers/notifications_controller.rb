class NotificationsController < ApplicationController
  def index
    respond_to do |f|
      f.html do
        @notifications = current_user.notifications
          .order(created_at: :desc).page(params[:page]).per(10).load
      end
      f.json do
        @notifications = current_user.notifications.for_list
      end
    end
    Notification.where(id: @notifications.map(&:id)).read!
  end

  def show
    n = current_user.notifications.find(params[:id])
    n.save if n.click!

    path = n.path
    if path
      redirect_to path
    else
      redirect_to root_path, n.no_path_error # Get's deleted here too
    end
  end

  def count
    render json: { count: current_user.n_count }
  end
end