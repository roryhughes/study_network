class HomeController < ApplicationController
  skip_before_action :authenticate_user!, except: [:marked_as_nw, :show_user]

  def index
    if current_user
      # DASHBOARD
      @user = current_user
      @dash = true
      @notes = @user.notes.order(created_at: :desc).page(params[:page]).per(10).load
      @medals = @user.medals
      render 'dashboard'
    else
      # Landing page
      @no_container = true
      render 'index_no_user'
    end
  end

  def modal
    @is_modal = true
    if ['login', 'register', 'goals_info', 'delete_account'].include? params[:modal_type]
      render 'modal/' + params[:modal_type], layout: false
    else
      render text: '404 modal not found', status: 404
    end
  end

  def about
    @title = "About"
  end

  def changelog
    @title = "Changelog"
    @versions = $version_hsh["versions"]
    @upcoming = $version_hsh["upcoming"]
  end

  def beta
    render 'index'
  end

  def marked_as_nw
    @title = "Noteworthies"
    @notes = current_user.noteworthies.order("votes.created_at DESC").page(params[:page]).per(10).load
  end
end
