class Admin::NotesController < AdminController
  before_filter :find_model

  def show
    if @note.hidden?
      @hider = current_user if current_user.id == @note.hidden_by
      @hider ||= @note.hider
    end
    if @note.featured?
      @featurer = current_user if current_user.id == @note.featured_by
      @featurer ||= @note.featurer
    end
    render 'show', layout: @layout
  end

  def hide # Not the opposite of show :)
    @note.hide!(current_user, note_params[:hidden_reason])
    @note.unfeature! # Hiding a note unfeatures it
    if try_save('Note hidden.')
      @note.user.add_notification(
        @note,
        :note_hidden,
        "Your note: '#{@note.title.truncate(30)}', was hidden by an admin. "\
        "Click here to find out why."
      )
    end
  end

  def unhide
    @note.unhide!
    if try_save('Note unhidden.')
      @note.user.add_notification(
        @note,
        :note_hidden,
        "Your note: '#{@note.title.truncate(30)}', was unhidden."
      )
    end
  end

  def feature
    return redirect_to @note, notice: "Can't feature hidden note" if @note.hidden?
    @note.feature!(current_user)
    if try_save('Note now featured.')
      @note.user.add_notification(
        @note,
        :note_featured,
        "Well done! Your note '#{@note.title.truncate(30)}' was featured by "\
        "an admin. It will now appear on the front page of the site!"
      )
    end
  end

  def unfeature
    @note.unfeature!
    try_save('Note unfeatured.')
  end

private
  def find_model
    @note = Note.find_by_id(params[:id]) if params[:id]
  end

  def note_params
    params.require(:note).permit(:hidden_reason)
  end

  def try_save(msg)
    if @note.save
      redirect_to @note, notice: msg
    else
      render action: 'show'
      false
    end
  end
end