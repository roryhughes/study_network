class AdminController < ApplicationController
  before_action :must_be_admin

protected

  def must_be_admin
    return raise ActionController::RoutingError.new('Not Found') unless current_user.admin?
  end
end