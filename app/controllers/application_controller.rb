class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception if Rails.env.production?

  before_action :authenticate_user!
  before_action :check_if_modal
  before_action :turbolinks?

  MODALS = {
    'sessions' =>      ['new'],
    'registrations' => ['new', 'delete_account', 'notifications'],
    'goals' =>         ['info'],
    "notes" =>         ['forks'],

    # Admin
    'admin/notes' =>   ['show']
  }

  helper_method :n_count, :turbolinks?, :show_n?

  def turbolinks?
    @turbolinks ||= request.headers["X-XHR-Referer"]
  end

  def n_count
    @n_count ||= current_user.n_count
  end

  def show_n?
    !@turbolinks && n_count > 0
  end

protected

  def devise_parameter_sanitizer
    if resource_class == User
      UserParameterSanitizer.new(User, :user, params)
    else
      super
    end
  end

  # Modals
  def in_modal_list?(c, a)
    MODALS[params[:controller]].include? params[:action]
  end

  def check_if_modal
    # if params[:m] == '1' && request.post? && in_modal_list?(params[:controller], params[:action])
    if params[:m] == '1' && in_modal_list?(params[:controller], params[:action])
      @is_modal = true
    else
      @layout = 'application'
    end
  end

  def modal_only
    return raise ActionController::RoutingError.new('Not Found') unless @is_modal
  end

  # def no_layout_check
  #   !(params[:no_layout] || request.headers["X-Requested-With"] == "XMLHttpRequest") && 'application'
  # end
end
